# Copyright (C) 2006, 2007 Free Software Foundation, Inc.
# translation of kio_jabberdisco.po to Macedonian
#
# Bozidar Proevski <bobibobi@freemail.com.mk>, 2006.
msgid ""
msgstr ""
"Project-Id-Version: kio_jabberdisco\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2019-05-20 03:10+0200\n"
"PO-Revision-Date: 2006-01-11 17:06+0100\n"
"Last-Translator: Bozidar Proevski <bobibobi@freemail.com.mk>\n"
"Language-Team: Macedonian <mkde-l10n@lists.sourceforge.net>\n"
"Language: mk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 1.11\n"
"Plural-Forms: Plural-Forms: nplurals=3; plural=n%10==1 ? 0 : n%10==2 ? 1 : "
"2;\n"

#: jabberdisco.cpp:96 jabberdisco.cpp:183
#, kde-format
msgid "TLS"
msgstr "TLS"

#: jabberdisco.cpp:166
#, kde-format
msgid "The server certificate is invalid. Do you want to continue? "
msgstr "Сертификатот на серверот не е валиден. Дали сакате да продолжите? "

#: jabberdisco.cpp:167
#, kde-format
msgid "Certificate Warning"
msgstr "Предупредување за сертификат"

#: jabberdisco.cpp:284
#, kde-format
msgid "The login details are incorrect. Do you want to try again?"
msgstr "Деталите за најава не се исправни. Дали сакате да се обидете повторно?"
